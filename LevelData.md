first:
collision stuff far above `3A5C` 
second (0x801F)
`3A5C`-`3A5D`: 1 uint16 (timer current value)
`3A5E`-`3A5F`: 1 uint16 (timer total time value)
`3A86`-`3A87` : 1 uint16 (current level id)
`3A7F` `3A7F` : 1 uint8 (current banana count)
...
`3C60`-`3C60` : 1 uint8 (face button input)
`3C61`-`3C61` : 1 uint8 (triggers bottom hex, and top D-Pad)
`3C62`-`3C63` : 2 uint8 (x and y axis of main stick)
`3C63`-`3C64` : 2 uint8 (x and y axis of c stick)
...
`3DC0`~`3E10` : (minimap info)